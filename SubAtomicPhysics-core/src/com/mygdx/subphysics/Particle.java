package com.mygdx.subphysics;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.utils.Array;

/*
 * 1qc = 5.33E-20c
 * 1qm = .8768E-15m
 * 1qkg = 1.67E-27kg
 * 1qJ = 1.28E-57J 
 * 1qN = 1.46E-42N
 * G = 2.69E-29qmqmqm/(qkgss)
 * K = 2.27E43
 */
public abstract class Particle
{
	public static final double GRAV_CONST = new Double("2.69E-29");
	public static final double COUL_CONST = new Double("-2.27E43");

	public final Vector3d position;
	protected final Vector3d velocity;
	protected final Vector3d acceleration;
	protected final Vector3d force;
	protected final double mass;
	protected final double charge;
	private boolean updated = false;

	public ModelInstance model;

	public Particle(Model model, Vector3d position, double mass, double charge)
	{
		this.position = position.cpy();
		this.model = new ModelInstance(model);
		this.model.transform.setTranslation(position.toV3());
		this.velocity = new Vector3d();
		this.acceleration = new Vector3d();
		this.force = new Vector3d();
		this.mass = mass;
		this.charge = charge;
	}

	protected Vector3d getDisplacement(Particle other)
	{
		return new Vector3d(other.position.x - position.x, other.position.y - position.y,
				other.position.z - position.z);
	}

	protected Vector3d temp = new Vector3d();

	public void updateForce(Array<Particle> particles)
	{
		force.set(0, 0, 0);
		updated = true;
		for (Particle p : particles)
		{
			if (p != this)
			{
				force.add(applyForce(p));
			}
		}
	}

	protected Vector3d applyForce(Particle particle)
	{
		return applyGravity(particle).add(applyElectricForce(particle));
	}

	protected Vector3d applyGravity(Particle other)
	{
		Vector3d dir = getDisplacement(other);
		return dir.scl(mass * other.mass / (dir.len2() * dir.len()));
	}

	public Vector3d applyElectricForce(Particle other)
	{
		Vector3d dis = getDisplacement(other);
		return dis.scl(COUL_CONST * charge * other.charge / (dis.len2() * dis.len()));
	}

	public abstract void print();

	public void update(double deltaTime)
	{
		if (updated)
		{
			acceleration.set(force).scl(1 / mass);
			velocity.add(temp.set(acceleration).scl(deltaTime));
			position.add(temp.set(velocity).scl(deltaTime));
			model.transform.setTranslation(position.toV3());
			updated = false;
		}
	}

	public void render(ModelBatch modelBatch, Environment env)
	{
		modelBatch.render(model, env);
	}

	public String toString()
	{
		return position + "," + velocity + "," + acceleration + "," + force;
	}

	public String forceDirection()
	{
		return force.cpy().toString();
	}
}
