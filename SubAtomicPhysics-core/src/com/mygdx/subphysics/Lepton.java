package com.mygdx.subphysics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;

public class Lepton extends Particle
{
	public enum Type
	{
		Electron, Positron;
		
		public double getCharge()
		{
			return this == Electron ? -3.0 : 3.0;
		}
		
		public Color getColor()
		{
			return this == Electron ? Color.GOLD : Color.SKY;
		}
	}
	
	public static Model elecModel;
	public Lepton(Vector3d position, Type type)
	{
		super(elecModel, position, 5.45E-4, type.getCharge());
		for (Material material : super.model.materials)
		{
			material.set(ColorAttribute.createDiffuse(type.getColor()));
		}
	} 
	public void print()
	{
		
	}
}
