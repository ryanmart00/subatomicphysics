package com.mygdx.subphysics;

import java.io.Serializable;

import com.badlogic.gdx.math.Vector3;

public class Vector3d implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3662450053977421292L;
	public final static Vector3d Zero = new Vector3d(); 
	
	public double x;
	public double y;
	public double z;

	public Vector3d()
	{
		set(0.0, 0.0, 0.0);
	}

	public Vector3d(double x, double y, double z)
	{
		set(x, y, z);
	}
	
	public Vector3d (String string) throws NumberFormatException
	{
		String x = null, y = null, z = null;
		int start = 1;
		if (string.length() == 0)
		{
			throw new NumberFormatException();
		}
		if (string.charAt(0) != '(' || string.charAt(string.length() - 1) != ')')
		{
			throw new NumberFormatException();
		}
		for(int i = 1; i < string.length(); i++)
		{
			if (string.charAt(i) == ',' || string.charAt(i) == ')')
			{
				if (x == null)
				{
					x = string.substring(start, i);
				}
				else if (y == null)
				{
					y = string.substring(start, i);
				}
				else if (z == null)
				{
					z = string.substring(start, i);
				}
				else
				{
					throw new NumberFormatException();
				}
				start = i + 1;
			}
		}
		this.x = new Double(x);
		this.y = new Double(y);
		this.z = new Double(z);
	}

	public Vector3d(final Vector3d vector)
	{
		this.set(vector);
	}

	public Vector3d add(double num)
	{
		return set(x + num, y + num, z + num);
	}

	public Vector3d add(double x, double y, double z)
	{
		return set(this.x + x, this.y + y, this.z + z);
	}

	public Vector3d add(Vector3d other)
	{
		return add(other.x, other.y, other.z);
	}

	public Vector3d sub(double num)
	{
		return set(x - num, y - num, z - num);
	}

	public Vector3d sub(double x, double y, double z)
	{
		return set(this.x - x, this.y - y, this.z - z);
	}

	public Vector3d sub(Vector3d other)
	{
		return sub(other.x, other.y, other.z);
	}

	public Vector3d scl(double amt)
	{
		return set(x * amt, y * amt, z * amt);
	}

	public Vector3d set(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}

	public Vector3d set(Vector3d other)
	{
		return set(other.x, other.y, other.z);
	}

	public double len()
	{
		return Math.sqrt(len2());
	}

	public double len2()
	{
		return x * x + y * y + z * z;
	}

	public Vector3d crs(double x, double y, double z)
	{
		return set(this.y * z - this.z * y, this.z * x - this.x * z, this.x * y - this.y * x);
	}

	public Vector3d crs(Vector3d other)
	{
		return crs(other.x, other.y, other.z);
	}

	public double dot(double x, double y, double z)
	{
		return this.x * x + this.y * y + this.z * z;
	}

	public double dot(Vector3d other)
	{
		return dot(other.x, other.y, other.z);
	}

	public Vector3d cpy()
	{
		return new Vector3d(this);
	}

	public Vector3 toV3()
	{
		return new Vector3((float) x, (float) y, (float) z);
	}

	public String toString()
	{
		return "(" + x + "," + y + "," + z + ")";
	}

	public Vector3d nor()
	{
		final double len2 = this.len2();
		if (len2 == 0.0 || len2 == 1.0)
			return this;
		return this.scl(1.0 / Math.sqrt(len2));
	}
	

}
