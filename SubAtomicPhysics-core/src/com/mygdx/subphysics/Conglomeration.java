package com.mygdx.subphysics;

import com.badlogic.gdx.utils.Array;

public class Conglomeration
{
	private Particle[] objects;
	private Vector3d position = new Vector3d();
	public int len;
	
	public Conglomeration(Array<Particle> p, Particle... particles)
	{
		objects = particles;
		for(Particle part : particles)
		{
			if(!p.contains(part, true)){p.add(part);}
		}
		setPosition();
		len = objects.length;
	}
	
	public Vector3d getPosition()
	{
		setPosition();
		return position;
	}
	
	private void setPosition()
	{
		position.set(0, 0, 0);
		for(Particle p : objects)
		{
			position.add(p.position);
		}
		position.scl(1.0/ objects.length);
	}
	

}
