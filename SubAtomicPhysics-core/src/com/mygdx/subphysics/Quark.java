package com.mygdx.subphysics;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.MathUtils;

public class Quark extends Particle
{

	public enum Flavor
	{
		Up, Down;

		public String toString()
		{
			return this == Up ? "Up" : "Down";
		}
	}

	public enum QColor
	{
		Red, Green, Blue, Antired, Antigreen, Antiblue;

		public static Vector3d red = new Vector3d(0.0, -1.0, 0.0);
		public static Vector3d blue = new Vector3d(Math.sqrt(3) / 2.0, .5, 0.0);
		public static Vector3d green = new Vector3d(-Math.sqrt(3) / 2.0, .5, 0.0);

		public Color getColor()
		{
			return this == Red ? Color.RED
					: this == Blue ? Color.BLUE
							: this == Green ? Color.GREEN
									: this == Antired ? Color.CYAN : this == Antiblue ? Color.YELLOW : Color.MAGENTA;
		}

		public QColor absoluteValue()
		{
			return this == Red || this == Antired ? Red : this == Blue || this == Antiblue ? Blue : Green;
		}

		public Vector3d getVector()
		{
			QColor abs = absoluteValue();
			return (abs == Red ? red : (abs == Blue ? blue : green)).cpy().scl(getState());
		}

		private QColor getNext()
		{
			return this == Red ? Blue
					: this == Blue ? Green
							: this == Green ? Red : this == Antired ? Antiblue : this == Antiblue ? Antigreen : Antired;
		}

		private QColor getPrior()
		{
			return this == Red ? Green
					: this == Blue ? Red
							: this == Green ? Blue
									: this == Antired ? Antigreen : this == Antiblue ? Antired : Antiblue;
		}

		public QColor getAnti()
		{
			return this == Red ? Antired
					: this == Blue ? Antiblue
							: this == Green ? Antigreen
									: this == Antired ? Red : this == Antiblue ? Blue : Green;

		}

		public QColor pickRandom()
		{
			return (int) (MathUtils.random() * 2) == 0 ? getNext() : getPrior();
		}

		public int getState()
		{
			return this == Red || this == Blue || this == Green ? 1 : -1;
		}

		public String toString()
		{
			return this == Red ? "Red" : (this == Blue ? "Blue" : "Green");
		}
	}

	public static Model model;
	public static final double H_BAR_C = new Double("2.816E51");// should be
																// 2.816E51
	public static final double ALPHA_S = -H_BAR_C / 2f;
	public static final double B = new Double("3.638E45");// calculated to
															// 3.638E40

	protected final Flavor flavor;
	protected final QColor color;

	public Quark(Vector3d position, Flavor flavor, QColor color)
	{
		super(model, position, (flavor == Flavor.Up ? .0025 : .0051), (flavor == Flavor.Up ? 2 : -1));
		this.flavor = flavor;
		this.color = color;
		for (Material material : super.model.materials)
		{
			material.set(ColorAttribute.createDiffuse(color.getColor()));
		}
	}

	public void update(double deltaTime)
	{
		super.update(deltaTime);
	}

	public Flavor getFlavor()
	{
		return flavor;
	}

	public QColor getColor()
	{
		return color;
	}

	public String toString()
	{
		return "Quark: { " + super.toString() + ", " + flavor + ", " + "}";
	}

	private static double len;
	private boolean p = false;

	private Vector3d applyColorForce(Quark other)
	{
		if (p)
		{
			System.out.println();
			p = false;
		}
		double scale = Math.round(color.getVector().scl(-1).dot(other.color.getVector()) * 2.0) / 2.0;
//		if (scale < 0)
//		{
//			scale = 0;
//		}
		Vector3d dir = getDisplacement(other);
		len = dir.len();
		dir.nor();
//		return Vector3d.Zero.cpy();
		return dir.scl(scale * ((ALPHA_S / (len * len) + B)));
	}

	public void print()
	{

		if (color == QColor.Antired)
			p = true;
	}

	@Override
	public Vector3d applyForce(Particle particle)
	{
		if (particle instanceof Quark)
		{
			return super.applyForce(particle).add(applyColorForce((Quark) particle));
		}
		return super.applyForce(particle);
	}
}
