package com.mygdx.subphysics;

import com.badlogic.gdx.Input.TextInputListener;

public class MyTextListener implements TextInputListener
{
	private StringWrapper string;
	public MyTextListener(StringWrapper string)
	{
		this.string = string;
	}
	
	@Override
	public void input(String text)
	{
		string.string = text.toLowerCase();	
	}

	@Override
	public void canceled()
	{		
	}

}
