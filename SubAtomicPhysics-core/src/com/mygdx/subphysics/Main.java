package com.mygdx.subphysics;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

// Using Libgdx by badlogicgames for graphics and user interface

public class Main extends ApplicationAdapter
{

	private ModelBatch modelBatch;
	private PerspectiveCamera camera;
	private CameraInputController inputcontroller;
	private ModelBuilder builder;
	private Environment environment;
	private Array<Conglomeration> objects = new Array<Conglomeration>();
	private Array<Particle> particles = new Array<Particle>();
	public double deltaTime = new Double("5E-23");
	private StringWrapper string = new StringWrapper();
	private MyTextListener listener;

	private int num = 0;

	@Override
	public void create()
	{
		modelBatch = new ModelBatch();
		builder = new ModelBuilder();
		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
		Quark.model = builder.createSphere(50f, 50f, 50f, 10, 10,
				new Material(ColorAttribute.createDiffuse(Color.WHITE)), Usage.Normal | Usage.Position);
		Lepton.elecModel = builder.createSphere(25f, 25f, 25f, 10, 10, new Material(ColorAttribute.createDiffuse(Color.GOLD)), Usage.Normal | Usage.Position);
		camera = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.near = .5f;
		camera.far = 10000000;
		camera.lookAt(Vector3.Zero.cpy());
		camera.update();
		listener = new MyTextListener(string);
		inputcontroller = new CameraInputController(camera);
		Gdx.input.setInputProcessor(inputcontroller);

	}

	public void createPion(Vector3d position, Quark.QColor color, boolean anti)
	{
		if (anti)
		{
			objects.add(new Conglomeration(particles,
					new Quark(new Vector3d(-350 + position.x, position.y, position.z), Quark.Flavor.Up, color),
					new Quark(new Vector3d(350 + position.x, position.y, position.z), Quark.Flavor.Up,
							color.getAnti())));
		}
		else
		{
			objects.add(new Conglomeration(particles,
					new Quark(new Vector3d(-350 + position.x, position.y, position.z), Quark.Flavor.Down, color),
					new Quark(new Vector3d(350 + position.x, position.y, position.z), Quark.Flavor.Down,
							color.getAnti())));
		}
	}

	public void createProton(Vector3d position, boolean anti)
	{
		if (!anti)
		{
			objects.add(new Conglomeration(particles,
					new Quark(new Vector3d(500 + position.x, position.y, position.z), Quark.Flavor.Up,
							Quark.QColor.Red),
					new Quark(new Vector3d(position.x, position.y, 500 + position.z), Quark.Flavor.Up,
							Quark.QColor.Blue),
					new Quark(new Vector3d(position.x, 500 + position.y, position.z), Quark.Flavor.Down,
							Quark.QColor.Green)));
		}
		else
		{
			objects.add(new Conglomeration(particles,
					new Quark(new Vector3d(500 + position.x, position.y, position.z), Quark.Flavor.Up,
							Quark.QColor.Antired),
					new Quark(new Vector3d(position.x, position.y, 500 + position.z), Quark.Flavor.Up,
							Quark.QColor.Antiblue),
					new Quark(new Vector3d(position.x, 500 + position.y, position.z), Quark.Flavor.Down,
							Quark.QColor.Antigreen)));
		}
	}
	
	public void createElectron(Vector3d position, boolean anti)
	{
		if (!anti)
		{
			objects.add(new Conglomeration(particles,
					new Lepton(position, Lepton.Type.Electron)));
		}
		else
		{
			objects.add(new Conglomeration(particles,
					new Lepton(position, Lepton.Type.Positron)));
		}
	}

	public void createNeutron(Vector3d position, boolean anti)
	{
		if (!anti)
		{
			objects.add(new Conglomeration(particles,
					new Quark(new Vector3d(500 + position.x, position.y, position.z), Quark.Flavor.Up,
							Quark.QColor.Red),
					new Quark(new Vector3d(position.x, position.y, 500 + position.z), Quark.Flavor.Down,
							Quark.QColor.Blue),
					new Quark(new Vector3d(position.x, 500 + position.y, position.z), Quark.Flavor.Down,
							Quark.QColor.Green)));
		}
		else
		{
			objects.add(new Conglomeration(particles,
					new Quark(new Vector3d(500 + position.x, position.y, position.z), Quark.Flavor.Up,
							Quark.QColor.Antired),
					new Quark(new Vector3d(position.x, position.y, 500 + position.z), Quark.Flavor.Down,
							Quark.QColor.Antiblue),
					new Quark(new Vector3d(position.x, 500 + position.y, position.z), Quark.Flavor.Down,
							Quark.QColor.Antigreen)));
		}
	}

	private Vector3 temp = new Vector3();
	private void updateCam(int num)
	{
		try
		{
			camera.lookAt(objects.get(num).getPosition().toV3());
			camera.translate(temp.set(camera.direction).scl(objects.get(num).getPosition().toV3().sub(camera.position).len() - 2000f));
		}
		catch (IndexOutOfBoundsException e)
		{
		}
		camera.update();
	}

	private boolean pause = true;
	private boolean nextFrame;
	private boolean print = false;

	@Override
	public void pause()
	{
		pause = true;
	}
	
	@Override
	public void render()
	{
		if ((Gdx.input.isKeyJustPressed(Keys.ENTER)))
		{
			Gdx.input.getTextInput(listener, "Input", "", "Input a value");
		}
		if (Gdx.input.isKeyPressed(Keys.ESCAPE))
		{
			Gdx.app.exit();
		}
		if (Gdx.input.isKeyJustPressed(Keys.P))
		{
			pause = !pause;
		}
		if (Gdx.input.isKeyJustPressed(Keys.N))
		{
			nextFrame = true;
		}
		if (Gdx.input.isKeyJustPressed(Keys.S))
		{
			print = true;
		}
		if (string.string.length() > 7 && string.string.substring(0, 6).equals("change"))
		{
			try
			{
				num = Integer.parseInt(string.string.substring(7, string.string.length()));
			}
			catch (NumberFormatException e)
			{
			}
		}
		else if (string.string.length() > 9 && string.string.substring(0, 10).equals("add proton"))
		{
			try
			{
				createProton(new Vector3d(string.string.substring(11, string.string.length())), false);
			}
			catch (Exception e)
			{
			}
		}
		else if (string.string.length() > 11 && string.string.substring(0, 12).equals("add electron"))
		{
			try
			{
				createElectron(new Vector3d(string.string.substring(13, string.string.length())), false);
			}
			catch (Exception e)
			{
			}
		}
		else if (string.string.length() > 11 && string.string.substring(0, 12).equals("add positron"))
		{
			try
			{
				createElectron(new Vector3d(string.string.substring(13, string.string.length())), true);
			}
			catch (Exception e)
			{
			}
		}
		else if (string.string.length() > 13 && string.string.substring(0, 14).equals("add antiproton"))
		{
			try
			{
				createProton(new Vector3d(string.string.substring(15, string.string.length())), false);
			}
			catch (Exception e)
			{
			}
		}
		else if (string.string.length() > 10 && string.string.substring(0, 11).equals("add neutron"))
		{
			try
			{
				createNeutron(new Vector3d(string.string.substring(12, string.string.length())), false);
			}
			catch (Exception e)
			{
			}
		}
		else if (string.string.length() > 14 && string.string.substring(0, 15).equals("add antineutron"))
		{
			try
			{
				createNeutron(new Vector3d(string.string.substring(16, string.string.length())), true);
			}
			catch (Exception e)
			{
			}
		}
		string.string = "";
		updateCam(num);

		for (int i = 0; i < particles.size; i++)
		{
			particles.get(i).updateForce(particles);
		}

		Gdx.gl.glClearColor(.25f, .25f, .25f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		modelBatch.begin(camera);
		for (final Particle p : particles)
		{
			if (print)
			{
				p.print();
			}
			p.render(modelBatch, environment);
			if (nextFrame)
			{
				p.update(deltaTime);
			}
			else if (!pause)
			{
				p.update(deltaTime * Gdx.graphics.getDeltaTime());
			}
		}
		modelBatch.end();
		nextFrame = false;
		print = false;
	}

	public void dispose()
	{
		Quark.model.dispose();
	}
}